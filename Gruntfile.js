var mustache = require('mustache');

module.exports = function(grunt){

	 // Load grunt tasks automatically
	  require('load-grunt-tasks')(grunt);

	  // Time how long tasks take. Can help when optimizing build times
	  require('time-grunt')(grunt);

	  grunt.initConfig({

	    watch: {
	      js: {
	        files: ['app/src/**/*.js'],
	        options: {
	          livereload: true
	        }
	      },
	      livereload: {
	        options: {
	          livereload: '<%= connect.options.livereload %>'
	        },
	        files: [
	          'app/templates/*.html',
	          'templates/index.html',
	          'app/styles/*.css',
	          'app/src/*.js',
	          'app/styles/images/*.{png,jpg,jpeg,gif,webp,svg}'
	        ]
	      }
	    },
	    connect: {
	      options: {
	        port: 9898,
	        hostname: 'localhost',
	        livereload: 35729
	      },
	      livereload: {
	        options: {
	          open: true,
	          base: ['app']
	        }
	      }
	    }
	  });

	grunt.registerTask('buildfiles', function(){

		var filesJS = grunt.file.expand("app/src/*.js");
    	var filesCSS = grunt.file.expand("app/styles/*.css");

		var files_renamedJS = filesJS.filter(function(file){
			return !file.match('app.js');
		}).map(function(file){
			return file.replace('app/', '');
		});

	    var files_renamedCSS = filesCSS.map(function(file){
	      return file.replace('app/', '');
	    });

		var template = grunt.file.read('templates/index.html');
		var rendered = mustache.render(template, {filesJS:files_renamedJS, filesCSS:files_renamedCSS});

		grunt.file.write('app/index.html', rendered);

	});

	grunt.registerTask('server', function() {
	    grunt.task.run([
	      'buildfiles',
	      'connect:livereload',
	      'watch'
	    ]);
	  });
};