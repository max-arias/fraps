app.directive('chart', function(){
    return{
        restrict: 'E',
        require: 'ngModel',
        link: function(scope, elem, attrs, ngModel){
            
            var dataset, options;

            var dataset = [{ 
				label: "FPS", 
				data: null
			}];

			var options = {
		        series: {
		            lines: {
		                show: true
		            },
		            threshold: {
						below: scope.lowerLimit,
						color: 'red'
					}
		        }
		    }

			scope.$watch(attrs.ngModel, function (newData, oldData) {
				if(newData != oldData){

					if(newData.length > 2000){
						options.series.downsample = {threshold: 1500} //Downsample for speed
					}

					dataset[0].data = newData;
					plotChart();
				}
			});

			scope.$watch('lowerLimit', function (newData, oldData) {
				if(newData != oldData){
					options.series.threshold.below = newData;
					plotChart();
				}
			});

			var plotChart = function(){
				setTimeout(function(){ //Flot needs the element to be visible. A ms is enough to render correctly.
					$.plot(elem, dataset, options);
				},1);
			}



        }
    };
});

app.directive('dropZone', function(){
	return{
		restrict: 'A',
		link: function(scope, element) {

			//Issue with jQuery and dataTransfer:
			jQuery.event.props.push('dataTransfer');

			element.bind('dragover', processDragOverOrEnter);

			var fpsLogNameExpression = new RegExp(/(PlanetSide2_(x64|x86?) [0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}-[0-9]{2}-[0-9]{2}-[0-9]{2} fps.csv)+/)
			var frapsLogName = 'FRAPSLOG.TXT';

			function processDragOverOrEnter(evt) {
				evt.stopPropagation();
				evt.preventDefault();
				evt.dataTransfer.dropEffect = 'copy';
			}

        	element.bind('drop', function(evt){

        		var files = evt.dataTransfer.files;
        		var foundFile = false;

        		evt.stopPropagation();
				evt.preventDefault();
				scope.clearError();

				for (var j = 0, file; file = files[j]; j++) {
					if(fpsLogNameExpression.test(file.name) === true){
						foundFile = true;
					}
				}

				if(!foundFile){
					scope.setDropError();
					return false;
				}

				for (var i = 0, f; f = files[i]; i++) {

					if(fpsLogNameExpression.test(f.name) === true){
						var reader = new FileReader();

						reader.onload = (function(theFile) {
							return function(e) {
								var result_data = e.currentTarget.result;
								scope.setFpsLogData(result_data);
							};
						})(f);

						reader.readAsText(f);
					}
					
				}

				return false;
        	});
        }
	}
})