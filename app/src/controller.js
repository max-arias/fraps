'use strict';

var mainModule = angular.module('mainModule', []);

mainModule.controller('mainCtrl', function ($scope, $http) {
	$scope.frapsData;

	$scope.fraps_log;
	$scope.fps_log;
	$scope.lowerLimit = 30;
	$scope.totalSamples;
	$scope.sampleAverage;

	$scope.error;

	$scope.$watch('fraps_log', function(newValue, oldValue) {
		if(newValue){
			$scope.fraps_log = $scope.fraps_log.split("\n");
			console.log($scope.fraps_log);
		}
	});

	$scope.$watch('fps_log', function(fpsData) {
		if(fpsData){

			var data = fpsData;


			var totalSum = _.reduce(data, function(a, b){ return a + b; }, 0);
			var average  = totalSum / data.length;
			var maxVal   = average * 2; 
			var minVal   = average / 2;

			$scope.totalSamples  = fpsData.length;
			$scope.sampleAverage = average.toFixed(2);

			var outData = [];
			for(var i = 0; i < fpsData.length; i++){



				if(fpsData[i] > maxVal || fpsData[i] < minVal) continue;

				outData.push([i, fpsData[i]]);
			}

			$scope.frapsData = outData;
		}
	});

	$scope.setFpsLogData = function(data){
		var data_arr = data.split("\n");
		data_arr = data_arr.map(Number);
		data_arr = data_arr.filter(Boolean);

		$scope.$apply($scope.fps_log = data_arr);
	}

	$scope.setDropError = function(){
		$scope.error = 'Please drop both "FRAPSLOG.TXT" and "PlanetSide2_x64 YYYY-MM-DD XX-XX-XX-XX fps.csv" on the dropzone';
		$scope.$apply($scope.error);
	}

	$scope.clearError = function(){
		$scope.error = null;
	}

});
