'use strict';

var app = angular.module('frapsRead', ['ngRoute', 'mainModule', 'ui-rangeSlider']);

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.

      when('/', {
        templateUrl: 'templates/main.html',
        controller: 'mainCtrl'
      }).

      otherwise({
        redirectTo: '/',
      });

  }]);
